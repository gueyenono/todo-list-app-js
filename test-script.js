// Get today's date
const today = new Date();

// Compare today with October 3rd
// if (today.getDate() === 3 && today.getMonth() === 9) {
//   console.log("It's October 3rd.");
// } else {
//   console.log("It's not October 3rd.");
// }

console.log(today.toLocaleDateString("en-US", {weekday: "long", month: "short", day: "numeric"}))
