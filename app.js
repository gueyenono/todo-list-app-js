document.addEventListener("DOMContentLoaded", () => {

  // const today = new Date()
  // const dateOptions = {weekday: "long", month: "short", day: "numeric"}
  // const currentDate = document.querySelector(".current-date h1")
  // currentDate.textContent = today.toLocaleDateString("en-US", dateOptions)
  //
  // const addButton = document.querySelector(".add-button")

  // Class: Todo ----
  class Todo {
    constructor(todo) {
      this.todo = todo
    }
  }

  // Class: UI (Handles UI tasks)

  class UI {
    static displayTodo() {

    }

    static addToDo(todo) {
      const list = document.querySelector("#list")
      const row = document.createElement("li")
      row.innerHTML = `
        <i></i>
        <p>${todo.todo}</p>
        <a href="#"><i class = fa fa-plus-circle></i></a>
      `
      list.appendChild(row)
    }

    static deleteTodo() {

    }

    static showAlert() {

    }

    static clearTodos() {

    }
  }

  // Class: Store (Handles local storage)

  class Store {

    static getTodos() {
      let todos
      if(localStorage.getItem("todos") == null){
        todos = []
      } else {
        todos = JSON.parse(localStorage.getItem("todos"))
      }
      return todos
    }

    static addTodo(todo) {
      const todos = Store.getTodos()
      todos.push(todo)
      localStorage.setItem("todos", JSON.stringify(todos))
    }

    static removeTodo() {

    }
  }

  // EVENT: Add a todo

  document.querySelector("#add-button").addEventListener("click", () => {

    // Validate
    const toDoText = document.querySelector("#to-do-text").value

    if(toDoText === ""){
      alert("You need to enter a task!")
    } else {

      // Instantiate todo
      const todo = new Todo(toDoText)

      // Add to-do to local storage
      Store.addTodo(todo)

      // Add to-do to list
      UI.addToDo(todo)
    }


  })

})
